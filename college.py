import school
class Course:
    def __init__(self, students):
        self.students = students
    
    def __repr__(self):
        return f'All the students enrolled in this course are: {self.students}'

aruvi = school.Student("Aruvi")
gayatri = school.Student("Gayatri")
students = [aruvi, gayatri,]
django = Course(students)

#capitals = {'Telangana': 'Hyderabad', 'Andhra Pradesh': 'Amaravati', 'Maharashtra': 'Mumbai'}
#print(capitals.get('Telangana', 'unknown'))
#print(capitals.get('Tamil Nadu', 'unknown'))
#print(django)

MAX_SPEED = 80

def should_get_ticket(speed):
    return speed > MAX_SPEED