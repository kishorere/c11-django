from django.urls import path, include
from . import views

urlpatterns = [
    path('secret', views.djangoauth_show_secret, name="show_secret")
]