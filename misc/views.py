from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

# Create your views here.

secret_colour = "PURPLE"

def show_secret(request):
    error_message = ''
    if request.method=='POST':
        username = request.POST.get('username', 'red')
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            error_message = "Invalid username"
        else:
            user = get_object_or_404(User, username=username)
            entered_pw = request.POST.get('password', '')
            if user.password == entered_pw:
                request.session['knows_secret'] = True
            else:
                error_message = "Invalid password"
                request.session['knows_secret'] = False
    context = {'error_message': error_message}
    context['knows_secret'] = bool(request.session.get('knows_secret', False))
    return render(request, 'misc/secret_content.html', context)

def djangoauth_show_secret(request):
    context = {}
    error_message = ''
    if request.method == "POST":
        username_ = request.POST.get('username')
        password_ = request.POST.get('password')
        user = authenticate(username=username_, password=password_)
        if user is not None:
            login(request, user)
            if not user.has_perm('misc.view_secret_page'):
                return render(request, 'misc/secret_content.html', context)
        else:
            error_message = "Invalid Password"
            request.session['knows_secret'] = False
    context = {'error_message': error_message}
    return render(request, 'misc/secret_content.html', context)