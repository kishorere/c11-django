#please map /hello/sayhello to a function say_hello.

from django.urls import path
#from .views import say_hello
from . import views
urlpatterns = [
    path('sayhello', views.say_hello, name='say_hello'),
    path('saygoodbye', views.say_goodbye, name='say_goodbye'),
    path('fibonacci', views.get_nth_fibonacci, name='nth_fibonacci'),
    path('fib/<int:n>', views.get_nth_fibonacci_alt, name='nth_fibonacci_alt'),
    path('fibform', views.nth_fibonacci_post, name='fib_form'),
    path('students/all', views.student_list, name='student_list'),
    path('students/<int:roll_no_>', views.student_detail, name='student_detail'),
    path('students/add', views.update_or_create_student, name='create_student'),
    path('students/<int:id_>/edit', views.update_or_create_student, name='edit_student'),
    path('students/<int:roll_no_>/delete', views.delete_student, name='delete_student'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout')
    
]